<?php

namespace App\Entity;

use App\Repository\PhoneRepository;

class Phone
{
    private $brand;
    private $name;
    private $capacity;
    private $cpu;
    private $price;

    public function __construct($brand, $name, $capacity, $cpu, $price) {
        $this->brand = $brand;
        $this->name = $name;
        $this->capacity = $capacity;
        $this->cpu = $cpu;
        $this->price = $price;
    }

    // utility
    public function getValue() {
        return $this->capacity / $this->price;
    }
    public function getFullName() {
        return $this->brand." ".$this->name;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function getCpu(): ?string
    {
        return $this->cpu;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }
}
