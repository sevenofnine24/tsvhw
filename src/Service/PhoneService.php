<?php

namespace App\Service;

use App\Entity\Phone;

class PhoneService
{
    private $phones;
    private $shuffle;

    public function __construct() {
        $this->phones = [];
        $this->shuffle = [];
        $this->readPhones();
        $this->shufflePhones(20);
    }

    public function getBase() {
        return $this->phones;
    }

    public function getExtra() {
        return $this->shuffle;
    }

    private function readPhones() {
        $file = fopen("../templates/phones.tsv", "r") or die("unable to open tsv.");
        while (!feof($file)) {
            $segments = explode("\t", fgets($file));
            array_push($this->phones, new Phone(
                $segments[0],
                $segments[1],
                intval($segments[2]),
                $segments[3],
                intval($segments[4])
            ));
        }
        fclose($file);
    }

    private function shufflePhones($size) {
        for ($i = 0; $i < $size; $i++) {
            $sizeof = sizeof($this->phones)-1;
            array_push($this->shuffle, new Phone(
                $this->phones[rand(0, $sizeof)]->getBrand(),
                $this->phones[rand(0, $sizeof)]->getName(),
                $this->phones[rand(0, $sizeof)]->getCapacity(),
                $this->phones[rand(0, $sizeof)]->getCpu(),
                $this->phones[rand(0, $sizeof)]->getPrice()
            ));
        }
    }
}