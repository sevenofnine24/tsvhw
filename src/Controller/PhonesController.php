<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\PhoneService;

class PhonesController extends AbstractController
{
    /**
     * @Route("/phones/", name="phones")
     * @param PhoneService $ps
     * @return Response
     */
    public function index(PhoneService $ps)
    {
        $base = $ps->getBase();
        $extra = $ps->getExtra();

        return $this->render("phones/index.html.twig", array_merge(
            $this->baseAnswers($base),
            $this->shuffleAnswers($base, $extra),
            ["phones" => $base]
        ));
    }

    /**
     * @Route("/phones/pdf", name="pdf")
     * @param PhoneService $ps
     * @return Response
     */
    public function pdf(PhoneService $ps) {
        $base = $ps->getBase();
        $extra = $ps->getExtra();

        return $this->render("phones/pdf.html.twig", array_merge(
            $this->baseAnswers($base),
            $this->shuffleAnswers($base, $extra)
        ));
    }

    private function baseAnswers($base) {
        $over_200k = 0;
        $storage_128gb = 0;
        $most_expensive = $base[0];
        $offered_count = [];
        $most_value = $base[0];
        $index = 0;
        foreach ($base as $phone) {
            if ($phone->getPrice() > 200000) $over_200k++;
            if ($phone->getCapacity() == 128) $storage_128gb++;
            if ($phone->getPrice() > $most_expensive->getPrice()) $most_expensive = $phone;
            if ($phone->getValue() > $most_value->getValue()) $most_value = $phone;

            $str_cap = strval($phone->getCapacity());
            if (array_key_exists($str_cap, $offered_count)) $offered_count[$str_cap]++;
            else $offered_count[$str_cap] = 1;

            $index++;
        }

        // min of offered
        $least_offered = null;
        foreach ($offered_count as $key => $value) {
            if ($least_offered == null) $least_offered = $key;
            if ($offered_count[$least_offered] > $value) $least_offered = $key;
        }

        return [
            "most_expensive" => $most_expensive,
            "least_offered" => $least_offered,
            "least_offered_cnt" => $offered_count[$least_offered],
            "best_value" => $most_value,
            "over_200k" => $over_200k,
            "storage_128gb" => $storage_128gb
        ];
    }

    private function shuffleAnswers($base, $extra) {
        // intersect
        $intersect = [];
        foreach ($base as $bp) {
            $found = false;
            foreach ($extra as $ep) {
                if ($bp->getFullName() == $ep->getFullName())
                    $found = true;
            }
            if ($found) array_push($intersect, $bp);
        }

        // union
        $union = $base + $extra;

        // avg apple
        $apple_price_sum = 0;
        $apple_cnt = 0;
        foreach ($union as $ep) {
            if ($ep->getBrand() == "Apple") {
                $apple_price_sum += $ep->getPrice();
                $apple_cnt++;
            }
        }

        // a14
        $a14 = [];
        foreach ($union as $ep) {
            if ($ep->getCpu() == "A14 Bionic")
                array_push($a14, $ep);
        }
        $a14_min = $a14[0];
        foreach ($a14 as $p) {
            if ($a14_min->getPrice() > $p->getPrice())
                $a14_min = $p;
        }

        return [
            "avg_apple" => $apple_price_sum / $apple_cnt,
            "cnt_apple" => $apple_cnt,
            "intersect_cnt" => sizeof($intersect),
            "a14_bionic" => $a14_min
        ];
    }
}
